const assert = require('assert');
const { ConfigService } = require('../src/config-service')

// function () vs anonymous () => {}
// this

describe('Config Service', () => {
  let startEnv;
  let startConfigService;
  beforeEach(function(){
    startEnv = process.env;
    startConfigService = new ConfigService({env: startEnv})
  })

  it('should return object of value when TEST_ENV exists in process.env', () => {
    process.env['TEST_ENV_2'] = 'abc'
    const spec = { testEnv: { env: 'TEST_ENV_2' } }
    const expectValue = { testEnv: 'abc' }

    startConfigService = new ConfigService(startEnv)
    const result = startConfigService.resolve(spec);

    assert.deepEqual(result,  expectValue)
  })

  it('should return empty string when TEST_ENV not exists in process.env and default not specified', function (done) {
    process.env['TEST_ENV2'] = 'abc';
    const spec = { testEnv: { env: 'TEST_ENV' } };
    const expectValue = { testEnv: '' };

    const result = startConfigService.resolve(spec);

    assert.deepEqual(result,  expectValue)
    done()
  })

  it('should return object of value in integer decimal when TEST_ENV exists in process.env and mapper type is number and no decimal point', function (done) {
    process.env['TEST_ENV'] = '12345';
    const spec = { testEnv: { env: 'TEST_ENV', type: 'number' } };
    const expectValue = { testEnv: '12345' };

    const result = startConfigService.resolve(spec);

    assert.deepEqual(result,  expectValue)
    done()
  })

  afterEach(function(){
    process.env = Object.assign(startEnv);
  });
});

/*

//process.env
{ TEST_ENV: 'abc' }

input
{ testEnv: { env: 'TEST_ENV' } }

output
{ testEnv: 'abc' }

- should return object of value when TEST_ENV exists in process.env
GIVEN process.env = { TEST_ENV: 'abc' }
INPUT { testEnv: { env: 'TEST_ENV' } }
Expected OUTPUT = { testEnv: 'abc' }

- should return empty string when TEST_ENV not exists in process.env and default not specified
GIVEN process.env = { TEST_ENV2: 'abc' }
INPUT { testEnv: { env: 'TEST_ENV' } }
Expected OUTPUT = { testEnv: '' }


- should return object of value when TEST_ENV not exists in process.env and default is defined
GIVEN process.env = { TEST_ENV2: 'abc' }
INPUT { testEnv: { env: 'TEST_ENV', default: 'test' } }
Expected OUTPUT = { testEnv: 'test' }

- should return object of value in integer decimal when TEST_ENV exists in process.env and mapper type is number and no decimal point
GIVEN process.env = { TEST_ENV: '12345' }
INPUT { testEnv: { env: 'TEST_ENV', type: 'number' } }
Expected OUTPUT = { testEnv: 12345 }

- should return object of value in float when TEST_ENV exists in process.env and mapper type is number and have decimal point
GIVEN process.env = { TEST_ENV: '12345.123' }
INPUT { testEnv: { env: 'TEST_ENV', type: 'number' } }
Expected OUTPUT = { testEnv: 12345.123 }

- should throw error when TEST_ENV exists in process.env and mapper type is number but value isn't number
GIVEN process.env = { TEST_ENV: 'a1' }
INPUT { testEnv: { env: 'TEST_ENV', type: 'number' } }
Expected OUTPUT = throw error 'Environment variable value is not a number'

- should return object of value in array when TEST_ENV exists in process.env and mapper type is array
GIVEN process.env = { TEST_ENV: 'a,b,c' }
INPUT { testEnv: { env: 'TEST_ENV', type: 'array' } }
Expected OUTPUT = { testEnv: ['a','b','c'] }

- should return object of value when TEST_ENV exists in process.env and mapper type is string
GIVEN process.env = { TEST_ENV: 'test }
INPUT { testEnv: { env: 'TEST_ENV' } }
Expected OUTPUT = { testEnv: 'test'}

*/

