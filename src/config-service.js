/**
 *  ## System under test: ConfigService
 *
 *  ### Observable behaviors:
 *
 *  #### Class constructor
 *        - Merge the key-value input with the current state of process environment variables.
 *
 *  #### Method: `resolve(spec)`
 *    - Input is dictionary of configuration spec:
 *    - Method will resolve the value of environment variables by key in spec.[*].env
 *    - Method will append the value of resolved environment variable into result object with the key defined in configuration spec.
 *    - If environment variable does not exist by key, set the value to empty string by default, otherwise if default value is provided, (spec.[*].default is defined), set the value to the spec.
 *
 *    - Type inference of configuration values
 *        - If the type of resolved value is not defined (spec.[*].type is undefined), return value in string by default
 *        - If the type of resolved value is number (spec.[*].type is number), cast the value into integer or float. (99 as integer, 99.999 as float)
 *        - If the type of resolved value is array (spec.[*].type is array), create the list of values delimited by string.  (‘a , b , c’  -> [‘a’, ‘b’, ‘c’])
 *        - For numeric type, if the converted value is not a number, raise the exception.
 */
class ConfigService {
  constructor(env) {
    process.env = { ...process.env, ...env }
  }

  /**
   * spec: {
   *   [key: string]: {
   *     env: string,
   *     default?: string | number
   *     type?: string
   *   }
   * }
   *
   * output {
   *   [key: string]: any
   * }
   */
  resolve(spec) {
    const config = {}

    for (const [ key, mapper ] of Object.entries(spec)) {
      const defaultValue = mapper.default ?? ''
      const value = process.env[mapper.env]?.trim() ?? defaultValue

      if (mapper.type === 'number') {
        const parsedValue = value.indexOf('.') < 0 ? parseInt(value, 10) : parseFloat(value)
        if (isNaN(parsedValue)) {
          throw new TypeError('Environment variable value is not a number')
        }
        config[key] = parsedValue
      } else if (mapper.type === 'array') {
        config[key] = value.split(',').map(v => v.trim())
      } else {
        config[key] = value
      }
    }

    return config
  }
}

module.exports = { ConfigService }
